
def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(word, int = 2)
  repeat = []
    int.times do
      repeat << word
    end
repeat.join(" ").to_s
end

def start_of_word(letters, num)
  letters.slice(0...num)
end

def first_word(string)
  string.split[0]
end

def titleize(word)
  little_words = ["and", "or", "the", "are", "other", "over"]
  caps = []
  word.split.each_with_index do |w, idx|
    if !little_words.include?(w)
      caps << w.capitalize
    elsif idx == 0
      caps << w.capitalize
    else
      caps << w
    end
  end
caps.join(" ").to_s
end
