def translate(string)
  pig_sentence = []

  string.split.each do |word|
    pig_sentence << latinize(word)
  end

  pig_sentence.flatten.join(" ")
end


def latinize(word)
  alphabet = ("a".."z").to_a
  vowels = ("aeiou").split("")
  consonants = alphabet - vowels
  pig_word = []

  if vowels.include?(word[0])
    pig_word << word + "ay"
  elsif word[0] == "q" && word[1] == "u"
    pig_word << word[2..-1] + "quay"
  elsif consonants.include?(word[0]) && (word[1] == "q" && word[2] == "u")
    pig_word << word[3..-1] + word[0] + "quay"
  elsif consonants.include?(word[0]) && (consonants.include?(word[1]) && consonants.include?(word[2]))
    pig_word << word[3..-1] + word[0..2] + "ay"
  elsif consonants.include?(word[0]) && consonants.include?(word[1])
    pig_word << word[2..-1] + word[0..1] + "ay"
  elsif consonants.include?(word[0])
    pig_word << word[1..-1] + word[0] + "ay"
  end
pig_word
end
