def add(int, int_1)
  int + int_1
end

def subtract(int, int_1)
  int - int_1
end

def sum(arr)
  x = 0
  if arr.length == 0
    x
  else
    arr.inject (:+)
  end
end

def multiply(arr)
  arr.inject(:*)
end

def power(int, int_1)
  new_int = int
  int_1.times do
    new_int = new_int * int
  end
new_int/int
end

def factorial(int)
  if int > 0
    range = (1..int).to_a
    range.inject(:*)
  else
    int
  end
end
